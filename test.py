# from palindromeCheck import PalindromeCheck
from palindromeCheck import palindrome_check
import unittest
# from unittest.mock import patch

class MyTest(unittest.TestCase):
    def test_true(self):
        self.assertEqual(palindrome_check("bob"), True)
    def test_false(self):
        self.assertEqual(palindrome_check("mud"), False)
    def test_special_chars(self):
        self.assertEqual(palindrome_check("Do nine men interpret? Nine men I nod."), True)
        
if __name__ == '__main__':
    unittest.main()