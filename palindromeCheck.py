import re

# User inputs word to be checked and checks that the length of the input is more than 1 character
def word_input(word = ""):
    if word == "":
        word = str(input("Please enter a word(s): "))
    while len(word) < 2:
        print("Requires more than 1 character!")
        word = str(input("Please enter a word(s): "))
    return word

# Removes all special characters and spaces from the input
def format_word(word):
    word = re.sub('[^A-Za-z]+', '', word)
    return word

# Checks that the word is a palindrome
def word_match(word):
    fword = format_word(word).lower()
    reverse_word = fword
    reverse_word = reverse_word[::-1] 
    if fword == reverse_word:
        return True
    else:
        return False

# Runs all other functions and prints out whether or not the word entered is a palindrome
def palindrome_check(word = ""):
    word = word_input(word)
    if word_match(word) == True:
        print(word + " is a palindrome")
        return True
    else:
        print(word + " is not a palindrome")
        return False

if __name__ == "__main__":
    palindrome_check()